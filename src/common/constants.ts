export const DATA_ROUTER_MENU = {
  SYSTEM: 'system',
  INFORMATION: "information",
  GOTEST: "gotest",
  LIST_MANAGEMENT: "management"
}

export enum ENUM_ACCOUNT_TYPE {
  NONE = 0,
  Teacher = 1, // giao vien trung tam
  Student = 2,
  CTV = 3,
  Manager = 4,
  Admin = 5,
  Classroom_Management_Teacher = 7, // giao vien quan li lop
  Parrent = 10,
  EduManager = 11,
  EduManager_School = 12, // = EduManager + ManagerLevel + 1
  EduManager_Phong = 13,
  EduManager_So = 14,
  EduManager_Bo = 15,
}

export const STATUS = {
  SUCCESS: 1,
  WAITING: 0,
  CHANGE_INFORMATION: 4,
  BLOCK_ACCOUNT: 3,
};