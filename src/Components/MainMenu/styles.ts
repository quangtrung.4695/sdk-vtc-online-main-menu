import styled from "styled-components";

const ContentStyles = styled.div`
.custom-overlay-menu .ant-popover-inner {
  border-radius: 16px;
  box-shadow: 3px 0px 6px 0px rgba(0, 0, 0, 0.1);
  background: #f0f2f5;
}

.item-main-menu {
  width: 100%;
  padding: 8px;
  align-items: center;
  border-radius: 8px;
  cursor: pointer;
  border: 1px solid #9adfff;
}

.item-main-menu-default {
  background-color: #c1e7f8;
  cursor: default !important;
}

.title-item-main-menu {
  font-family: Roboto, Arial, Helvetica, sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  padding-left: 16px;
}

.img-main-menu {
  width: 24px;
  margin: 0 auto;
}

.content-main-menu {
  display: flex;
  flex-direction: row;
  column-gap: 8px;
  margin-top: 8px;
}

.title-main-menu {
  margin-top: 24px;
  font-size: 16px;
  font-weight: bold;
  display: flex;
  color: #1890ff;
}
`

export { ContentStyles }