import React, { memo, useEffect, useMemo, useState } from "react";
import clsx from "clsx";
import Tooltip from "antd/es/tooltip";
import { TItemDataMainMenu, getDataMainMenu } from "./DataRouter";
import { DATA_ROUTER_MENU, STATUS } from "../../common/constants";
import { getUserInfoResponse } from "../../types";
import { ContentStyles } from "./styles";

export type TMainMenuProps = {
  project: "quan-ly" | "gotest";
  currentRouterMenu?: string;
  GDPTUserInfor?: getUserInfoResponse;
  currentSubRouter?: string;
  onChangeRouter: (data: TItemDataMainMenu) => void;
};

const MainMenu = memo(({ project, currentRouterMenu, GDPTUserInfor, currentSubRouter, onChangeRouter }: TMainMenuProps) => {
  const [dataMainMenu, setDataMainMenu] = useState<TItemDataMainMenu[]>([]);

  useEffect(() => {
    if (currentSubRouter && GDPTUserInfor) {
      const newDataMainMenu = getDataMainMenu(currentSubRouter);
      const filterDataByUserEduType = newDataMainMenu.filter((item) => item.userEduType.includes(GDPTUserInfor.edu_user_type));
      if (filterDataByUserEduType && filterDataByUserEduType.length > 0) setDataMainMenu(filterDataByUserEduType[0].data);
    }
  }, [GDPTUserInfor, currentSubRouter, currentRouterMenu]);

  const handleRedirectRouter = (dataRouter: TItemDataMainMenu) => {
    if (dataRouter.keyRouter) {
      switch (project) {
        case "quan-ly":
          if (dataRouter.keyRouter === DATA_ROUTER_MENU.GOTEST) {
            const subRouter = window.location.origin + "/gotest";
            window.location.replace(subRouter);
          } else if (dataRouter.keyRouter !== currentRouterMenu) {
            onChangeRouter(dataRouter);
          }
          break;

        case "gotest":
          if (dataRouter.keyRouter !== DATA_ROUTER_MENU.GOTEST) {
            const subRouter = window.location.origin + "/quan-ly";
            window.location.replace(`${subRouter}${dataRouter.url}`);
          }
          break;

        default:
          break;
      }
    }
  };

  const approveTeacherSuccess = GDPTUserInfor?.status === STATUS.SUCCESS;
  const renderContent = useMemo(() => {
    return (
      <React.Fragment>
        {dataMainMenu.map((data) => (
          <Tooltip
            key={data.keyRouter}
            placement="top"
            title={data.title}
            color="white"
            overlayInnerStyle={{ color: "black", borderRadius: 8 }}
          >
            <div
              className={clsx("item-main-menu", {
                "item-main-menu-default": currentRouterMenu === data.keyRouter,
              })}
              key={data.keyRouter}
              onClick={() => handleRedirectRouter(data)}
            >
              <img
                className="img-main-menu"
                src={data.icon}
              />
            </div>
          </Tooltip>
        ))}
      </React.Fragment>
    );
  }, [dataMainMenu, currentRouterMenu]);

  const renderTitleMainMenu = (): string => {
    if (dataMainMenu && dataMainMenu.length > 0) {
      const filterTitleByRouterMenu = dataMainMenu.filter((item) => item.keyRouter === currentRouterMenu);
      return filterTitleByRouterMenu && filterTitleByRouterMenu.length > 0 ? filterTitleByRouterMenu[0].title : "";
    }
    return "";
  };

  if (!currentRouterMenu || dataMainMenu.length === 0) return <React.Fragment></React.Fragment>;

  return (
    <ContentStyles>
      <div className="content-main-menu">{approveTeacherSuccess && renderContent}</div>
      <span className="title-main-menu">{renderTitleMainMenu()}</span>
    </ContentStyles>
  );
});

export default MainMenu;
