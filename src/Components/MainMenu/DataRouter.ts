import { DATA_ROUTER_MENU, ENUM_ACCOUNT_TYPE } from "../../common/constants";
import PATH_MANAGER from "../../router/Manager/path";
import PATH_TEACHER from "../../router/Teacher/path";

export type TItemDataMainMenu = {
  title: string;
  keyRouter: string;
  icon: string;
  url: string;
};

type TDataMainMenu = {
  userEduType: ENUM_ACCOUNT_TYPE[];
  data: TItemDataMainMenu[];
};

const MainMenuListManagement = require("./assets/icon/main-menu-list-management.png");
const MainMenuBoard = require("./assets/icon/main-menu-exam-ioe.png");
const MainMenuExamIOEIcon = require("./assets/icon/main-menu-exam-ioe.png");
const MainMenuGotestIcon = require("./assets/icon/main-menu-gotest.png");

export const getDataMainMenu = (selCurrentSubRouter: string) => {
  return [
    ///////
    // Main router for Edu manager So and manager phong
    ///////
    {
      userEduType: [ENUM_ACCOUNT_TYPE.EduManager_So, ENUM_ACCOUNT_TYPE.EduManager_Phong],
      data: [
        {
          title: "Quản lý danh sách",
          keyRouter: DATA_ROUTER_MENU.LIST_MANAGEMENT,
          icon: MainMenuListManagement,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.LIST_MANAGEMENT + PATH_MANAGER.LOCAL_SCHOOL,
        },
        {
          title: "Quản lý thi IOE",
          keyRouter: DATA_ROUTER_MENU.SYSTEM,
          icon: MainMenuExamIOEIcon,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.SYSTEM + PATH_MANAGER.REGISTER_AND_CREATE_EXAM.ROOT_PATH,
        },
        {
          title: "Quản lý kỳ thi của bạn",
          keyRouter: DATA_ROUTER_MENU.GOTEST,
          icon: MainMenuGotestIcon,
          url: "",
        },
        {
          title: "Quản lý thông tin",
          keyRouter: DATA_ROUTER_MENU.INFORMATION,
          icon: MainMenuBoard,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.INFORMATION + PATH_MANAGER.PROFILE,
        },
      ],
    },
    ///////
    // Main router for Edu manager school
    ///////
    {
      userEduType: [ENUM_ACCOUNT_TYPE.EduManager_School],
      data: [
        {
          title: "Quản lý danh sách",
          keyRouter: DATA_ROUTER_MENU.LIST_MANAGEMENT,
          icon: MainMenuListManagement,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.LIST_MANAGEMENT + PATH_MANAGER.TEACHER_MANAGER,
        },
        {
          title: "Quản lý thi IOE",
          keyRouter: DATA_ROUTER_MENU.SYSTEM,
          icon: MainMenuExamIOEIcon,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.SYSTEM + PATH_MANAGER.MANAGER_STUDENT,
        },
        {
          title: "Quản lý kỳ thi của bạn",
          keyRouter: DATA_ROUTER_MENU.GOTEST,
          icon: MainMenuGotestIcon,
          url: "",
        },
        {
          title: "Quản lý thông tin",
          keyRouter: DATA_ROUTER_MENU.INFORMATION,
          icon: MainMenuBoard,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.INFORMATION + PATH_MANAGER.PROFILE,
        },
      ],
    },
    ///////
    // Main router for manager teacher
    ///////
    {
      userEduType: [ENUM_ACCOUNT_TYPE.Classroom_Management_Teacher],
      data: [
        {
          title: "Quản lý học sinh",
          keyRouter: DATA_ROUTER_MENU.LIST_MANAGEMENT,
          icon: MainMenuListManagement,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.LIST_MANAGEMENT + PATH_TEACHER.MANAGER_STUDENT,
        },
        {
          title: "Quản lý và giao bài tập",
          keyRouter: DATA_ROUTER_MENU.GOTEST,
          icon: MainMenuGotestIcon,
          url: "",
        },
        {
          title: "Quản lý thông tin",
          keyRouter: DATA_ROUTER_MENU.INFORMATION,
          icon: MainMenuBoard,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.INFORMATION + PATH_MANAGER.PROFILE,
        },
      ],
    },
    ///////
    // Main router for teacher
    ///////
    {
      userEduType: [ENUM_ACCOUNT_TYPE.Teacher],
      data: [
        {
          title: "Quản lý học sinh",
          keyRouter: DATA_ROUTER_MENU.LIST_MANAGEMENT,
          icon: MainMenuListManagement,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.LIST_MANAGEMENT + PATH_MANAGER.MANAGER_STUDENT,
        },
        {
          title: "Quản lý và giao bài tập",
          keyRouter: DATA_ROUTER_MENU.GOTEST,
          icon: MainMenuGotestIcon,
          url: "",
        },
        {
          title: "Quản lý thông tin",
          keyRouter: DATA_ROUTER_MENU.INFORMATION,
          icon: MainMenuBoard,
          url: selCurrentSubRouter + "/" + DATA_ROUTER_MENU.INFORMATION + PATH_MANAGER.PROFILE,
        },
      ],
    },
  ] as TDataMainMenu[];
};
