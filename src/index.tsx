import React from "react";
import MainMenu, { TMainMenuProps } from "./Components/MainMenu";

export const MainMenuComponent = ({ project, currentRouterMenu, GDPTUserInfor, currentSubRouter, onChangeRouter }: TMainMenuProps) => {
  return (
    <MainMenu
      project={project}
      currentRouterMenu={currentRouterMenu}
      currentSubRouter={currentSubRouter}
      GDPTUserInfor={GDPTUserInfor}
      onChangeRouter={onChangeRouter}
    />
  );
};

