const PATH_TEACHER = {
  MANAGER_STUDENT: '/quan-ly-hoc-sinh',
  REGISTER_STUDENT: '/dang-ky-thi-cac-cap',
  RESULT_EXAM: '/ket-qua-thi',
  TRAINING_SELF_RESULT: '/ket-qua-tu-luyen-khoi-vong'
}
export default PATH_TEACHER
