import React from "react";
import { MainMenuComponent } from "main-menu";

const App = () => {
  return (
    <MainMenuComponent
      project="quan-ly"
      onChangeRouter={(data) => console.log('data', data)}
      currentRouterMenu="exam"
      currentSubRouter="test"
      GDPTUserInfor={{
        PositionID: 3154,
        pk_user_id: 1296708745,
        AccountType: 1,
        s_username: "thtkgv",
        s_fullname: "Phí Thị Tiến Thơm",
        s_email: "thomptt@vtconline.vn",
        d_bith: "1994-07-22T00:00:00",
        s_mobile: "******3930",
        s_address: "Test tài khoản",
        i_status: 0,
        Sex: 0,
        d_create_date: "2022-11-07T12:00:55.13",
        fk_province_id: 6,
        fk_village_id: 114,
        Grade: 1,
        SchoolLevel: 1,
        fk_school_id: 4752,
        i_block: 0,
        s_class_name: "",
        EditClass: 1,
        lastLogin: "0001-01-01T00:00:00",
        s_tenIOE: "Phí Thị Tiến Thơm",
        s_province_name: "Bình Ðịnh",
        s_village_name: "Huyện Phù Cát",
        s_school_name: "Trường Tiểu học Số 2 Ân Tường Tây",
        Blockname: "",
        GradeName: "",
        ListServiceCode: "",
        serviceCode: [],
        OrderBy: 0,
        edu_user_type: 12,
        b_current_round: 1,
        UserEdu: 0,
        isExamed: false,
        infoOther: "",
        docPath: "",
        CanChangeInfo: false,
        CanChangeBlockInfo: true,
        MustChangeInfo: false,
        i_user_type: 1,
        status: 1,
      }}
    />
  );
};

export default App;
