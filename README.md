# main-menu

> made with main-menu

[![NPM](https://img.shields.io/npm/v/main-menu.svg)](https://www.npmjs.com/package/main-menu) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save main-menu
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'main-menu'
import 'main-menu/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [quangtrung4695](https://github.com/quangtrung4695)
